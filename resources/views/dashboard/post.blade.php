@extends('layouts.main')
@section('stylesheet')
<link type="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" />
<link type="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" />
@endsection

@section('content')
@if(Session::has('message'))
<p class="alert alert-info">{{ Session::get('message') }}</p>
@endif


<hr>
<a href="/post/create" class="float-right">Create Post</a>
<table id="example" class="table table-striped table-bordered" style="width:100%">
    <thead>
        <tr>
            <th>Post Name</th>
            <th>Post Description</th>
            <th>Category Name</th>
            <th>Tag Name</th>
            <th>Created Date</th>
            <th>Edit Post</th>
            <th>Delete Post</th>
        </tr>
    </thead>
    <tbody>
        @foreach($posts as $key=>$post)
        <tr>
            <td>{{$post['name']}}</td>
            <td>{{$post['description']}}</td>
            <td>{{$post['category_name']}}</td>
            <td>
                <?php if(!empty($post['post_tag_data'])){ ?>
            @foreach($post['post_tag_data'] as $k=>$poststag)
                 {{$poststag}}<br/>
            @endforeach
                <?php } ?>
            </td>
            <td>{{$post['created_at']}}</td>
            <th><a href="/post/{{$post['id']}}/edit">Edit</a></th>
            <th><form method="post" action={{route('post.store')}}>
                @csrf
              
                <input type="hidden" name="delete_row" value="yes">
                <input type="hidden" name="post_id" value="<?php if(!empty($post['id'])){echo $post['id'];} ?>">    
  
                <button type="submit" class="btn btn-primary">Delete</button>
            </form></th>
        </tr>
       @endforeach
        
    </tbody>
    <tfoot>
        <tr>
            <th>Post Name</th>
            <th>Post Description</th>
            <th>Category Name</th>
            <th>Tag Name</th>
            <th>Created Date</th>
            <th>Edit Post</th>
            <th>Delete Post</th>
        </tr>
    </tfoot>
</table>

@endsection

@section('script')
    <script src="https://code.jquery.com/jquery-3.3.1.js"  crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"  crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"  crossorigin="anonymous"></script>
@endsection
@extends('layouts.main')
@section('stylesheet')
<link type="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" />
<link type="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" />
@endsection

@section('content')
@if(Session::has('message'))
<p class="alert alert-info">{{ Session::get('message') }}</p>
@endif


<hr>
<a href="/tag/create" class="float-right">Create Tag</a>
<table id="example" class="table table-striped table-bordered" style="width:100%">
    <thead>
        <tr>
            <th>tag Name</th>
            <th>Created Date</th>
            <th>Edit tag</th>
            <th>Delete tag</th>
        </tr>
    </thead>
    <tbody>
        @foreach($tags as $tag)
        <tr>
            <td>{{$tag->name}}</td>
            <td>{{$tag->created_at}}</td>
            <th><a href="/tag/{{$tag->id}}/edit">Edit</a></th>
            <th><form method="post" action={{route('tag.store')}}>
                @csrf
              
                <input type="hidden" name="delete_row" value="yes">
                <input type="hidden" name="tag_id" value="<?php if(!empty($tag->id)){echo $tag->id;} ?>">    
  
                <button type="submit" class="btn btn-primary">Delete</button>
            </form></th>
        </tr>
       @endforeach
        
    </tbody>
    <tfoot>
        <tr>
            <th>tag Name</th>
            <th>Created Date</th>
            <th>Edit tag</th>
            <th>Delete tag</th>
        </tr>
    </tfoot>
</table>

@endsection

@section('script')
    <script src="https://code.jquery.com/jquery-3.3.1.js"  crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"  crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"  crossorigin="anonymous"></script>
@endsection
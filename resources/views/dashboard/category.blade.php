@extends('layouts.main')
@section('stylesheet')
<link type="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" />
<link type="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" />
@endsection

@section('content')
@if(Session::has('message'))
<p class="alert alert-info">{{ Session::get('message') }}</p>
@endif


<hr>
<a href="/category/create" class="float-right">Create Category</a>
<table id="example" class="table table-striped table-bordered" style="width:100%">
    <thead>
        <tr>
            <th>Category Name</th>
            <th>Created Date</th>
            <th>Edit Category</th>
            <th>Delete Category</th>
        </tr>
    </thead>
    <tbody>
        @foreach($categories as $category)
        <tr>
            <td>{{$category->name}}</td>
            <td>{{$category->created_at}}</td>
            <th><a href="/category/{{$category->id}}/edit">Edit</a></th>
            <th><form method="post" action={{route('category.store')}}>
                @csrf
              
                <input type="hidden" name="delete_row" value="yes">
                <input type="hidden" name="cat_id" value="<?php if(!empty($categories[0]->id)){echo $categories[0]->id;} ?>">    
  
                <button type="submit" class="btn btn-primary">Delete</button>
            </form></th>
        </tr>
       @endforeach
        
    </tbody>
    <tfoot>
        <tr>
            <th>Category Name</th>
            <th>Created Date</th>
            <th>Edit Category</th>
            <th>Delete Category</th>
        </tr>
    </tfoot>
</table>

@endsection

@section('script')
    <script src="https://code.jquery.com/jquery-3.3.1.js"  crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"  crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"  crossorigin="anonymous"></script>
@endsection
@extends('layouts.main')
@section('stylesheet')

<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

@endsection

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-8">
            <form method="post" action={{route('post.store')}}>
                @csrf
                <div class="form-group">
                <label for="exampleInputEmail1">Post Name</label>
                <input type="text" class="form-control" name="postname" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?php if(!empty($posts[0]->name)){echo $posts[0]->name;} ?>" placeholder="Enter post name">
                <label for="exampleInputEmail1">Post Description</label>
                <textarea type="text" class="form-control" name="postdescription" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter post description"><?php if(!empty($posts[0]->description)){echo $posts[0]->description;} ?></textarea>
            <input type="hidden" name="post_id" value="<?php if(!empty($posts[0]->id)){echo $posts[0]->id;} ?>">    
            </div>
                <div class="input-group mb-3">
                    <select class="custom-select" id="inputGroupSelect02" name="categoryname">
                        <option selected>Choose...</option>
                        @foreach ($categories as $category)
                            <option <?php if(!empty($posts[0]->category_id) && $category->id == $posts[0]->category_id){ echo 'selected';} ?> value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                  </div>
                  <div class="input-group mb-3">
                      @if(empty($tags_array))
                      <select class="form-control" name="tag[]" multiple="multiple">
                        @foreach($tags as $tag)
                            <option value={{ $tag->id }}>{{$tag->name}}</option>
                        @endforeach
                    </select>

                      @else
                            <select class="form-control" name="tag[]" multiple="multiple">
                                @foreach($tags as $tag)
                                    <option   <?php if (in_array($tag->id , $tags_array))
                                    {
                                    echo 'selected="selected"';
                                    } ?>value={{ $tag->id }}>{{$tag->name}}</option>
                                @endforeach
                            </select>
                        @endif
                      
                  </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        
    </div>
</div>


  @endsection
  @section('script')
  <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script>
        $(".js-example-tags").select2({
        tags: true
        });
    </script>
@endsection
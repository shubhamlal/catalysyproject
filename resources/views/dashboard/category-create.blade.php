@extends('layouts.main')


@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-8">
            <form method="post" action={{route('category.store')}}>
                @csrf
                <div class="form-group">
                <label for="exampleInputEmail1">Category Name</label>
                <input type="text" class="form-control" name="categoryname" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?php if(!empty($categories[0]->name)){echo $categories[0]->name;} ?>" placeholder="Enter category name">
                <input type="hidden" name="cat_id" value="<?php if(!empty($categories[0]->id)){echo $categories[0]->id;} ?>">    
            </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        
    </div>
</div>

  @endsection
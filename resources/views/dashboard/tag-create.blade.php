@extends('layouts.main')


@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-8">
            <form method="post" action={{route('tag.store')}}>
                @csrf
                <div class="form-group">
                <label for="exampleInputEmail1">Tag Name</label>
                <input type="text" class="form-control" name="name" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?php if(!empty($tags[0]->name)){echo $tags[0]->name;} ?>" placeholder="Enter tag name">
                <input type="hidden" name="tag_id" value="<?php if(!empty($tags[0]->id)){echo $tags[0]->id;} ?>"> 
                  
            </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        
    </div>
</div>

  @endsection
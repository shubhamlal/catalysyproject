<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
    public function posts()
    {
        return $this->belongsToMany('App\Post','post_tag','post_id');
    }

    public function createTag($name,$id=null,$delete_row=null){
        if(empty($id)){
        $tag = new Tag();
        }else{
            $tag = Tag::find($id);    
        }
        if(!empty($name)){
        $tag->name = $name;
        $tag->soft_delete = 1;  
        }
        if(!empty($delete_row)){
            $tag->soft_delete = 0;  
        }
        $tag->save();
    }

    public function findtag($id=null){
        $query = Tag::query();
        if(!empty($id)){
           $query->where('id',$id);
        }
        $query->where('soft_delete',1); 
       return $query->get();
       }

       public function findtagPost($Postid=null){
        $query = Tag::join('post_tag','post_tag.tag_id','=','tags.id')
        ->where('post_tag.post_id',$Postid)
        ->select('tags.id')->get()->toArray();
        return array_column($query, 'id');
       }
}

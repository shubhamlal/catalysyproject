<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post_tag extends Model
{

    protected $table = 'post_tag';
    protected $fillable = ['post_id','tag_id'];

    public function insertData($data){
        $postId =  !empty($data[0]['post_id']) ?$data[0]['post_id'] : 0;
        $res =  Post_tag::where('post_id', $postId)->delete();
         Post_tag::insert($data);
    }

    public function getData($post_id_array){

       return Post_tag::join('tags','post_tag.tag_id','=','tags.id')
        ->whereIn('post_tag.post_id',$post_id_array)->get()->toArray();
        
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use App\Post;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $Tag;
    public function __construct(Tag $Tag,Post $Post)
    {
        $this->tag = $Tag;
        $this->post = $Post;
    }
    public function index()
    {
        //
        $tags = $this->tag->findtag();
        
        return view('dashboard.tag',compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = $this->tag->findtag();
        $posts = $this->post->findpost();
        return view('dashboard.tag-create',compact('posts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        $tag = $this->tag->createTag($request->name,$request->tag_id,$request->delete_row);
        if(empty($request->tag_id)){
        return redirect('/tag')->with('message', 'Tag Added Successfully');
        }else if(!empty($request->delete_row)){
            return redirect('/tag')->with('message', 'Tag Deleted Successfully');
        }else {
            return redirect('/tag')->with('message', 'Tag Updated Successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         //
         $tags = $this->tag->findtag($id);
         $posts = $this->post->findpost();
         return view('dashboard.tag-create',compact('tags','posts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

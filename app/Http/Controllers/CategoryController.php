<?php

namespace App\Http\Controllers;
use App\Category;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $Category;
    public function __construct(Category $Category)
    {
        $this->category = $Category;
    }

    public function index()
    {
        //
        // $categories = Category::all();
        $categories = $this->category->findcategory();
        return view('dashboard.category',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       return view('dashboard.category-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        $category = $this->category->createCategory($request->categoryname,$request->cat_id,$request->delete_row);
        if(empty($request->cat_id)){
        return redirect('/category')->with('message', 'Category Added Successfully');
        }else if(!empty($request->delete_row)){
            return redirect('/category')->with('message', 'Category Deleted Successfully');
        }else {
            return redirect('/category')->with('message', 'Category Updated Successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $categories = $this->category->findcategory($id);
        // dd($categories);
        return view('dashboard.category-create',compact('categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;
use App\Category;
use App\Post;
use App\Tag;
use App\Post_tag;

use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $Post;
    protected $tag;
    public function __construct(Post $Post,Post_tag $Posttag,Tag $tag)
    {
        $this->post = $Post;
        $this->post_tag = $Posttag;
        $this->tag = $tag;
    }
    public function index()
    {
        $posts = $this->post->findpost();
        $post_ids = array_column($posts->toArray(), 'id');
        $post_tag_data = $this->post_tag->getData($post_ids);
        $posts = $this->post->formattingData($posts->toArray(),$post_tag_data);
        return view('dashboard.post',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::all();
        $tags = Tag::all();
        // dd($tags);
        return view('dashboard.create-post',compact('categories','tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        // dd($request->toArray());
        $post = $this->post->createPost($request);
        if(!empty($request->post_id)){
            $post   = $request->post_id;
        }
        if(!empty($post)){
            if(!empty($request->tag)){
                $tag_array = array();
                foreach($request->tag as $k=>$v){
                    $d = array();
                    $d['post_id'] = $post;
                    $d['tag_id'] = (int)$v;
                    $tag_array[] = $d;
                }
                $this->post_tag->insertData($tag_array);
               
            }
            
        }
        if(empty($request->post_id)){
        return redirect('/post')->with('message', 'Post Added Successfully');
        }else if(!empty($request->delete_row)){
            return redirect('/post')->with('message', 'Post Deleted Successfully');
        }else {
            return redirect('/post')->with('message', 'Post Updated Successfully');
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $posts = $this->post->findpost($id);
        $categories = Category::all();
        $tags = Tag::all();
        $tags_array = $this->tag->findtagPost($id);
       
        return view('dashboard.create-post',compact('categories','posts','tags','tags_array'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $fillable = ['name'];
    /**
     * Assign Category to posts.
     */
    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    public function createCategory($name,$id=null,$delete_row=null){
        if(empty($id)){
        $category = new Category();
        }else{
            $category = Category::find($id);    
        }
        if(!empty($name)){
        $category->name = $name;
        $category->soft_delete = 1;  
        }
        if(!empty($delete_row)){
            $category->soft_delete = 0;  
        }
        $category->save();
    }
    public function findcategory($id=null){
     $query = Category::query();
     if(!empty($id)){
        $query->where('id',$id);
     }
     $query->where('soft_delete',1); 
    return $query->get();
    }
}

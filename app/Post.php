<?php

namespace App;
use App\Tag;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    /**
     * Assign Category_id to Post.
     */
    public function categories()
    {
        return $this->belongsTo('App\Category','category_id');
    }
    public function tags()
    {
        return $this->belongsToMany('App\Tag','post_tag','tag_id');
    }
    public function createPost($postContent){

        // ->name,$request->description,$request->cat_id,$request->post_id,$request->delete_row
        if(empty($postContent->post_id)){
          $posts = new Post();
        }else{
          $id = $postContent->post_id;
          $posts = Post::find($id);    
        }
        
        if(!empty($postContent->delete_row)){
            $posts->soft_delete = 0;   
        }else{
            // dd($postContent->tag);
            $posts->name = $postContent->postname;
            $posts->description = $postContent->postdescription;
            $posts->category_id = $postContent->categoryname;
            $posts->soft_delete = 1;
            // foreach()   
        }
        $posts->save();
        if(!empty($posts->id)){
            return $posts->id;
        }else{
            return 0;
        }
       
    }

    public function findpost($id=null){
        $query = Post::join('categories','posts.category_id','=','categories.id');
        if(!empty($id)){
           $query->where('posts.id',$id);
        }
        $query->where('posts.soft_delete',1); 
        $query->select('posts.*','categories.name as category_name'); 
       return $query->get();
       }

       public function formattingData($postsData,$post_tag_data){
          
        foreach($post_tag_data as $k=>$v){
          $key = array_search($v['post_id'], array_column($postsData, 'id'));
          if($key !== false){
            $postsData[$key]['post_tag_data'][]= $v['name'];
          }
        }

       return $postsData;

       }
}
